<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<div class="container" style="border: 1px solid black;width:600px; background-color: blue">
    <br>
    <div style="border: 2px solid black;background-color:deepskyblue;color: black;margin-left: 30px;
    width: 500px">
        <p style="padding-left: 10px;"><b>Please Enter Book Name and Author Name</b></p>
    </div>

    <form class="form-horizontal" action="action_page.php" method="post"
          style="border: 2px solid black;alignment: center;background-color: deepskyblue;
     margin-left: 30px; width: 500px">

        <div class="col-xs-10" style="margin-left: 10px; margin-top: 10px">
        <div class="form-group">
            <label class="control-label col-sm-2" for="bookName">Book Name: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="bookName" placeholder="Enter Book Name">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="authorName">Author: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="authorName" placeholder="Enter Author Name">
            </div>
        </div>
            </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-md">Save</button>
                <button type="submit" class="btn btn-primary btn-md">Save and Add New</button>
                <button type="submit" class="btn btn-primary btn-md">Reset</button>
                <button type="submit" class="btn btn-primary btn-md">Back to List</button>
            </div>
        </div>
    </form>
    <br>
</div>

</body>
</html>